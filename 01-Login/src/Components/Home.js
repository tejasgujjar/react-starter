import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { user1_data_url, user2_data_url } from '../constants/ApiURLs';
import {get_inventory_data, checkout_data_action, add_item_to_cart} from '../actions';
import '../style/home.css';
import { Navbar, Button } from 'react-bootstrap';

class Home extends Component {
  constructor(props, context){
    super(props, context);
    this.renderInventoryList = this.renderInventoryList.bind(this);
    this.addToCart = this.addToCart.bind(this);
    console.log('user'+ this.props);
    this.state = {
      cart: [],
      cartCount: 0
    }
  }

  handleSubmit(e){
    e.preventDefault();
    // handle the submit action for the add to cart
  }

  goTo(route) {
    this.props.history.replace(`/${route}`)
  }
  componentWillMount(){
    console.log('Mounting home page', this.props);
    this.props.get_inventory_data('/get_inventories', this.props.history)
  }

  addToCart = (item) => (event) => {
    event.preventDefault();
    const {inventory} = this.props;
    const {inventory_cart} = inventory;
    const cart = [];
    console.log('showing carts:', inventory_cart);
    console.log("Pushing item to cart:", item);

    // inventory_cart.push(item)
    // add from the action to update reducer
    this.props.add_item_to_cart(item);
    // console.log(new_cart);
    // this.props.checkout_data_action(inventory_cart
  }

  renderInventoryList(item){
    return (
      <div className="inventory-card">
        <div>
          <h5>{item.name} - ({item.id})</h5>
          <p>{item.category}</p>
        </div>
        <div>
          ${item.price}
        </div>
        <div>
        <Button
          bsStyle="primary"
          id={item.id}
          onClick={this.addToCart(item)}
        >
          Add to cart
        </Button>
        </div>
      </div>
    );
  }

  render() {
    console.log('loaded home');
    const {inventory} = this.props;
    const {inventory_data, inventory_cart} = inventory;
    return (
      <div className="container test-css">
        <p>Total items in cart: {inventory_cart.length}</p>
        <div>
          {inventory_data.map((item) => <div>{this.renderInventoryList(item)}</div>)}
        </div>
      </div>
    );
  }
}

// export default Home
function mapStateToProps(state) {
  const { inventory } = state;
  return {
    inventory
  };
}
export default connect(mapStateToProps,{
  get_inventory_data,checkout_data_action,add_item_to_cart
})(Home);
