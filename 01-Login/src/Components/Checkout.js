import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { user1_data_url, user2_data_url } from '../constants/ApiURLs';
import {get_inventory_data} from '../actions';
import '../style/home.css';
import { Navbar, Button } from 'react-bootstrap';

class Checkout extends Component {
  constructor(props, context){
    super(props, context);
    this.renderInventoryList = this.renderInventoryList.bind(this);

  }
  componentWillMount(){
    console.log('Mounting home page', this.props);
    // this.props.get_inventory_data('/get_inventories', this.props.history)
  }
  // "id": 544,
  //     "name": "Shovel",
  //     "category": "Tools",
  //     "price": 13.99,
  //     "inventory": 25
  renderInventoryList(item){
    return (
      <div className="inventory-card">
        <div>
          <h5>{item.name} - ({item.id})</h5>
          <p>{item.category}</p>
        </div>
        <div>
          {item.price}$
        </div>
        <div>
        <Button
          bsStyle="primary"
        >
          Add to cart
        </Button>
        </div>
      </div>
    );
  }

  render() {
    console.log('loaded checkout');
    const {checkout} = this.props;
    const {checkout_data} = checkout;
    return (
      <div className="container test-css">
        <h3>Your shoppingn cart</h3>
        <div>
          {checkout_data.map((item) => <div>{this.renderInventoryList(item)}</div>)}
        </div>
      </div>
    );
  }
}

// export default Home
function mapStateToProps(state) {
  const { checkout } = state;
  return {
    checkout
  };
}
export default connect(mapStateToProps,{

})(Checkout);
// export default connect(mapStateToProps)(Home);
