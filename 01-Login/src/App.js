import React, { Component } from 'react';
import { Navbar, Button } from 'react-bootstrap';
import './App.css';

class App extends Component {
  constructor(props, context){
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
    this.goTo = this.goTo.bind(this);
  }
  goTo(route) {
    this.props.history.replace(`/${route}`)
  }
  handleClick = path => e => {
    e.preventDefault();
    console.log('Clicked nav bar buttons', path);
    this.goTo(path);
  }
  render() {
    return (
      <div>
        <Navbar fluid>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#">Test</a>
            </Navbar.Brand>
            <Button
              bsStyle="primary"
              className="btn-margin"
              onClick={this.handleClick('home')}
            >
              Home
            </Button>
            <Button
              bsStyle="primary"
              className="btn-margin"
              onClick={this.handleClick('checkout')}
            >
              Checkout
            </Button>
          </Navbar.Header>
        </Navbar>
      </div>
    );
  }
}

export default App;
