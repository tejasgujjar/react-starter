// import {
//   LOGIN_USER_SUCCESS,
//   LOGIN_USER_FAILURE
//  } from '../constants/ActionConstants';

const INIT_STATE = {
    checkout:[]
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case "CHECKOUT_DATA":
            return { ...state, checkout_data: action.payload };
  
        default: return { ...state };
    }
}
