// import {
//   LOGIN_USER_SUCCESS,
//   LOGIN_USER_FAILURE
//  } from '../constants/ActionConstants';

const INIT_STATE = {
    loading: false,
    inventory_data:[],
    inventory_cart:[]
};

// get set of the state.inventory_cart reducer data
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case "INVENTORY_DATA":
            return { ...state, inventory_data: action.payload };
        case "INVENTORY_DATA_FAILURE":
            return { ...state, loading:true  };
        case "checkout_inventory":
            return { ...state, inventory_cart: action.payload };
        case "add_cart_inventory":
            return { ...state, inventory_cart: state.inventory_cart.concat(action.payload)};
        default: return { ...state };
    }
}
