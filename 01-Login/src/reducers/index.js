import { combineReducers } from 'redux';
import inventory from './InventoryReducer';
import checkout from './CheckoutReducer';

const rootReducer = combineReducers({
  inventory,
  checkout
});

export default rootReducer;
