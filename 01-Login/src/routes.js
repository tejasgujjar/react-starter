import React from 'react';
import { Route, Router } from 'react-router-dom';
import App from './App';
import Home from './Components/Home';
import Checkout from './Components/Checkout';
import Auth from './Auth/Auth';
// import history from './history';
import { Provider } from 'react-redux';


const auth = new Auth();

export const makeMainRoutes = (history, store) => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <div>
          <Route path="/" render={(props) => <App auth={auth} {...props} />} />
          <Route path="/home" render={(props) => <Home auth={auth} {...props} />} />
          <Route path="/checkout" render={(props) => <Checkout auth={auth} {...props} />} />
        </div>
      </Router>
    </Provider>
  );
}
