import { callApi } from './ApiUtils';

export const get_inventory_data = (url, history) => (dispatch) => {
    // dispatch({ type: LOGIN_USER });
    const param = {}

    callApi(url, 'GET', dispatch, param)
        .then((success, data) => {
            console.log('success in getting inventory data');
            dispatch({ type: "INVENTORY_DATA", payload: success.data });
        })
        .catch((error, data) => {
            console.log('Error in getting  inventory data');
            dispatch({ type: "INVENTORY_DATA_FAILURE" });
        })
        .then(() => {
          console.log('Done executing axios');
          // dispatch({type: SHOW_SPINNER, payload:false});
        });
}

export const checkout_data_action = (item) => (dispatch) => {
    dispatch({type: "checkout_inventory", payload: item})
}

export const add_item_to_cart = (item) => (dispatch) => {
    dispatch({type: "add_cart_inventory", payload: item})
}
